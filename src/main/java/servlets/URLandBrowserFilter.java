package servlets;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.time.LocalTime;
import java.util.Enumeration;

public class URLandBrowserFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;

        System.out.println(servletRequest.getRemoteAddr());

        System.out.println(httpServletRequest.getRequestURL());

        printClientBrowser(httpServletRequest);

        int minute = LocalTime.now().getMinute();

//        if (minute % 2 == 0) {
//        } else {
//            httpServletRequest.getRequestDispatcher("WEB-INF/jsp/oddJSP.jsp").forward(servletRequest, servletResponse);
//        }
    }

    private void printHeaders(HttpServletRequest req) {

        Enumeration<String> headerNames = req.getHeaderNames();

        while (headerNames.hasMoreElements()) {

            String headerName = headerNames.nextElement();
            System.out.println(headerName);

            Enumeration<String> headers = req.getHeaders(headerName);

            while (headers.hasMoreElements()) {
                String headerValue = headers.nextElement();

                System.out.println(headerValue);
            }
        }
    }

    public void printClientBrowser(HttpServletRequest request) {
        System.out.println(request.getHeader("User-Agent"));
    }
}
