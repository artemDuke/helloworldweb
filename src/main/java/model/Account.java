package model;

import java.time.LocalTime;
import java.util.ArrayList;

public class Account {
    private int id;
    private String login_name;
    private int user_id;
    private String acc_name;
    private String email;
    private LocalTime registration_date;
    private String password;
    private ArrayList<Club> clubs;
    private Agent agent;

    public Account() {
    }
}
